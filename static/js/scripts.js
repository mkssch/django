$(document).ready(function () {
    $('.basket-items').html(JSON.parse(sessionStorage.getItem('data')));
    $('.count').html($('.basket-div').length);
    let form = $('#buying-product');
    console.log(sessionStorage);
    console.log(localStorage);
    form.on('submit', function (e) {
        e.preventDefault();
        let count = $('#count').val();
        if(count <= 0){
            alert('Количество товара не может быть меньше либо равно нулю');
            return false;
        }
        let submit_button = $('#submit_button');
        let product_id = submit_button.data('product_id');
        let product_name = submit_button.data('product_name');
        let product_price = submit_button.data('product_price');
        let product_image = $('.image img:first-child').attr('src');
        let basket = '<div class="basket-div"><a href=""><img src="'+product_image+'" alt style="width: 50px; height: 50px"></a><div>' +
            '<a class="href-name" href="">'+product_name+'</a><span>'+count+'</span><span>x</span><span>'+product_price+'</span></div>' +
            '<button class="add-item btn btn-success">v</button><button class="delete-item btn btn-danger">x</button></div>';
        $('.basket-items').append(basket);
        $('.count').html($('.basket-div').length);
        sessionStorage.setItem('data', JSON.stringify($('.basket-items').html()));



        let data = {};
        data.product_id = product_id;
        data.nmb = count;
        let url = form.attr('action');
        let csrf_token = $('#buying-product [name="csrfmiddlewaretoken"]').val();
        data['csrfmiddlewaretoken'] = csrf_token;
        console.log(data);
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: true,
            success: function () {
                console.log('ok')
            },
            error: function () {
                console.log('error')
            }
        });
    });


    let basket = '.basket-container';
    let basket_items = '.basket-items';

    function basketShowing() {
        $(basket_items).toggleClass('hidden');
    }


    $(document).on('click',basket, function (e) {
        e.preventDefault();
        basketShowing();
    });


    $(document).on('click', '.basket-div,' +basket_items  ,function (e) {
        e.stopPropagation();
    });
    $(document).on('click', '.delete-item', function (e) {
        e.stopPropagation();
        $(this).closest('div').remove();
        sessionStorage.setItem('data', JSON.stringify($('.basket-items').html()));
        $('.count').html($('.basket-div').length);
        if($('.basket-div').length == 0){
            $('.count').html('');
        }
    });
});