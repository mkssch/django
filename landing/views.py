from django.shortcuts import render
from products.models import ProductImage
import datetime

# Create your views here.


def home(request):
    product_popular = ProductImage.objects.filter(is_main=True, product__is_popular=True, product__is_active=True)

    product_new = ProductImage.objects.filter(product__is_active=True, is_main=True,
                                              product__created__lte=datetime.datetime.today()).order_by('-created')
    return render(request, 'landing/home.html', locals())

