from django.db import models
from django.db.models.signals import *
from PIL import Image


# Create your models here.


class ProductType(models.Model):
    name = models.CharField(max_length=64)
    is_active = models.BooleanField(default=True)
    slug_name = models.CharField(max_length=64)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тип товара'
        verbose_name_plural = 'Тип товаров'


class Product(models.Model):
    is_active = models.BooleanField(default=True)
    name = models.CharField(max_length=64)
    amount_in_warehouse = models.IntegerField()
    price_per_item = models.DecimalField(default=0, max_digits=10, decimal_places=2)
    product_type = models.ForeignKey(ProductType, blank=True, null=True, default=None, on_delete=models.CASCADE)
    discount = models.IntegerField(default=0)
    is_popular = models.BooleanField(default=False)
    description = models.TextField(blank=True, null=True, default=None)
    URL = models.CharField(verbose_name='URL', max_length=50, )
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def price_with_discount(self):
        price_with_discount = self.price_per_item - (self.price_per_item * self.discount / 100)
        return price_with_discount

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'


class ProductImage(models.Model):
    product = models.ForeignKey(Product, blank=True, null=True, default=None, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='static/media/products_images')
    is_main = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'

    def __str__(self):
        return self.product.name


def image_watermark_save(sender, instance, created, **kwargs):
    image_path = instance.image.url
    image_path = image_path[1:]
    watermark_image_path = 'static/img/watermark.png'

    base_image = Image.open(open(image_path, 'rb'))
    watermark_image = Image.open(watermark_image_path)
    width, height = base_image.size

    watermark_transparent = Image.new('RGBA', (width, height), (0, 0, 0, 0))
    watermark_transparent.paste(watermark_image, (width - 250, height - 250))  # 250 - размер watermark+50px

    position = (0, 0)
    base_image.paste(watermark_transparent, position, mask=watermark_transparent)
    base_image.save(image_path)


post_save.connect(image_watermark_save, sender=ProductImage)
