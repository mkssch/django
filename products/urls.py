from django.conf.urls import url, include
from products import views

urlpatterns = [
    url(r'^sneakers/(?P<product_id>[\w\-]+)/$', views.product, name='product'),
]


