from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import *
from orders.models import Order

# Create your models here.


class CustomUser(AbstractUser):
    username = models.CharField(max_length=32, unique=True)
    is_active = models.BooleanField(default=True)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=32)
    phone = models.CharField(max_length=6)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'Пользователя'
        verbose_name_plural = 'Пользователи'

