from django import forms
from django.forms import TextInput
from django.contrib.auth.forms import UserCreationForm
from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = CustomUser
        fields = ["email", "username"]
        labels = {"username": "Никнейм", "phone": "Номер телефона"}
        widgets = {
            'username': TextInput(attrs={'placeholder': 'Имя пользователя'}),
            'email': forms.EmailInput(attrs={'placeholder': 'Почта'}),
        }
