from django.conf.urls import url, include
from users import views
from django.views.generic.base import TemplateView


urlpatterns = [
    #url(r'^login', views.get_login_or_register_info, name='login'),
    url(r'^account/', include('django.contrib.auth.urls')),
    url(r'^account/signup/', views.SignUp.as_view(), name='signup'),
    url(r'^account/$', TemplateView.as_view(template_name='login_home.html')),
]
