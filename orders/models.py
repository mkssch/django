from django.db import models
from products.models import Product
from django.db.models.signals import post_save
# Create your models here.


class Status(models.Model):
    name = models.CharField(max_length=16, blank=True, null=True, default=None)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статус'

    def __str__(self):
        return self.name


class Order(models.Model):
    customer_email = models.EmailField()
    customer_name = models.CharField(max_length=64, blank=True, null=True, default=None)
    customer_mobile = models.CharField(max_length=48, blank=True, null=True, default=None)
    customer_address = models.CharField(max_length=48, blank=True, null=True, default=None)
    total_price = models.DecimalField(default=0, max_digits=20, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    comments = models.TextField(blank=True, null=True, default=None)

    def __str__(self):
        return "Заказ %s " % self.customer_email

    class Meta:
        verbose_name = 'Общий заказ'
        verbose_name_plural = 'Общие заказы'


class ProductOrder(models.Model):
    order = models.ForeignKey(Order, blank=True, null=True, default=None, on_delete=models.CASCADE)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, blank=True, null=True, default=None, on_delete=models.CASCADE)
    amount = models.IntegerField(default=1)
    total_price = models.DecimalField(default=0, max_digits=20, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        verbose_name = 'Подробности заказа товара'
        verbose_name_plural = 'Подробности заказа товаров'

    def save(self, *args, **kwargs):
        price_per_item = self.product.price_per_item
        if(self.product.discount > 0):
            self.total_price = self.amount * Product.price_with_discount(self.product)
        else:
            self.total_price = self.amount * price_per_item
        super(ProductOrder, self).save(*args, **kwargs)


def total_price_post_save(instance, **kwargs):
    order = instance.order
    all_products_in_order = ProductOrder.objects.filter(order=order, product__is_active=True)

    amount = 0
    order_total_price = 0
    for item in all_products_in_order:
        order_total_price += item.total_price
        amount += item.amount

    instance.product.amount_in_warehouse = instance.product.amount_in_warehouse - amount
    instance.order.total_price = order_total_price
    instance.order.save(force_update=True)
    instance.product.save(force_update=True)


post_save.connect(total_price_post_save, sender=ProductOrder)
